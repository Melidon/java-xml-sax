package xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class BusStopLister extends BusStopLogger {

	private ArrayList<BusStop> busStopList;
	double lat, lon;

	public BusStopLister(double lat, double lon) {
		super();
		this.busStopList = new ArrayList<BusStop>();
		this.lat = lat;
		this.lon = lon;
	}

	@Override
	protected void foundOne() {
		this.busStopList.add(this.currentBusStop);
	}

	public void printBusStops() {
		Collections.sort(this.busStopList, new Comparator<BusStop>() {

			public double dist1(double lat1, double lon1, double lat2, double lon2) {
				double R = 6371000; // metres
				double phi1 = Math.toRadians(lat1);
				double phi2 = Math.toRadians(lat2);
				double dphi = phi2 - phi1;
				double dl = Math.toRadians(lon2 - lon1);
				double a = Math.sin(dphi / 2) * Math.sin(dphi / 2)
						+ Math.cos(phi1) * Math.cos(phi2) * Math.sin(dl / 2) * Math.sin(dl / 2);
				double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				double d = R * c;
				return d;
			}

			@Override
			public int compare(BusStop bs1, BusStop bs2) {
				return (int) (dist1(lat, lon, bs1.getLat(), bs1.getLon())
						- dist1(lat, lon, bs2.getLat(), bs2.getLon()));
			}

		});
		
		for(BusStop busStop : this.busStopList) {
			this.printBusStop(busStop);
		}
	}

}
