package xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class BusStopLogger extends DefaultHandler {

	protected BusStop currentBusStop;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if (qName.equals("node")) {
			this.currentBusStop = new BusStop();
			double lat = Double.parseDouble(attributes.getValue("lat"));
			double lon = Double.parseDouble(attributes.getValue("lon"));
			this.currentBusStop.setLat(lat);
			this.currentBusStop.setLon(lon);
		} else if (qName.equals("tag")) {
			String key = attributes.getValue("k");
			String value = attributes.getValue("v");
			if (this.currentBusStop.isValid() == false) {
				if (key.equals("highway") && value.equals("bus_stop")) {
					this.currentBusStop.setValid(true);
				}
			} else {
				switch (key) {
				case "name":
					this.currentBusStop.setName(value);
					break;
				case "old_name":
					this.currentBusStop.setOldName(value);
					break;
				case "wheelchair":
					this.currentBusStop.setWheelchair(value);
					break;
				default:
					break;
				}

			}
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		if (qName.equals("node") && this.currentBusStop.isValid()) {
			this.foundOne();
		}
	}

	protected void foundOne() {
		this.printBusStop(this.currentBusStop);
	}

	protected void printBusStop(BusStop busStop) {
		System.out.println("Meg�ll�:\n\tN�v: " + busStop.getName() + " (" + busStop.getOldName() + ")\n\tKerekessz�k: "
				+ busStop.getWheelchair());
	}

}
