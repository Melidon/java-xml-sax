package xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Main {

	public static void main(String[] args) {
		// Argumentumok kezelése
		String filename = null;
		double lat = 0, lon = 0;
		for (int i = 0; i < args.length; ++i) {
			switch (args[i]) {
			case "-i":
				filename = args[++i];
				break;
			case "-lat":
				lat = Double.parseDouble(args[++i]);
				break;
			case "-lon":
				lon = Double.parseDouble(args[++i]);
				break;
			default:
				break;
			}
		}

		// Előkészület
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser praser = null;
		try {
			praser = factory.newSAXParser();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Első feladat
		boolean first = false;
		if (first) {
			TagCounter counter = new TagCounter();
			try {
				praser.parse(new java.io.File(filename), counter);
			} catch (Exception e) {
				e.printStackTrace();
			}
			counter.printTagCounter();
		}

		// Második feladat
		boolean second = false;
		if (second) {
			BusStopLogger logger = new BusStopLogger();
			try {
				praser.parse(new java.io.File(filename), logger);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Harmadik feladat
		boolean third = true;
		if (third) {
			BusStopLister lister = new BusStopLister(lat, lon);
			try {
				praser.parse(new java.io.File(filename), lister);
			} catch (Exception e) {
				e.printStackTrace();
			}
			lister.printBusStops();
		}
	}

}
