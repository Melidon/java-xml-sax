package xml;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class TagCounter extends DefaultHandler {

	private Dictionary<String, Integer> tagCounter;

	public TagCounter() {
		this.tagCounter = new Hashtable<String, Integer>();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		Integer amount = this.tagCounter.get(qName);
		if (amount == null) {
			amount = 0;
		}
		this.tagCounter.put(qName, amount + 1);
	}

	public void printTagCounter() {
		Enumeration<String> e = this.tagCounter.keys();
		while (e.hasMoreElements()) {
			String key = e.nextElement();
			Integer value = this.tagCounter.get(key);
			System.out.println(key + ": " + value);
		}
	}

}
